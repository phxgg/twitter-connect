# twitter-connect

Universis Instant Messaging connector for Twitter

> __Note:__
> Under development. The documentation is not complete.

## ToDo

- [ ] Create Authorization Code Flow logic in `routers/twitterConnectRouter.js`. View [Twitter OAuth Docs](https://developer.twitter.com/en/docs/authentication/api-reference/request_token)
- [ ] *If needed*, Connect to Twitter Bot in `TwitterConnectService.js`.
- [ ] Implement `sendDirectMessage` in `TwitterConnectService.js`.
