/* eslint-disable no-unused-vars */
import path from 'path';
import { Router } from 'express';
import { HttpBadRequestError, HttpError, HttpUnauthorizedError } from '@themost/common';
import fetch from 'node-fetch';
import { Passport } from 'passport';
import OAuth2Strategy from 'passport-oauth2';
import rateLimit from 'express-rate-limit';
import cookieSession from 'cookie-session';
import { ExpressDataApplication } from '@themost/express';
import { TwitterConnectService } from '../TwitterConnectService';

class InvalidOrExpiredTokenError extends HttpUnauthorizedError {
  constructor(msg) {
    super(msg || 'invalid/expired token');
    this.status = 401;
    this.code = '1040';
  }
}

/**
 * 
 * @param {import('express').Response} res - The response object
 * @param {number} statusCode - HTTP status code
 * @param {string} view - View name
 * @param {boolean} ok - Whether the request was successful
 * @param {any} html - HTML context
 * @param {any} model - Model
 * @param {Error} error - Error
 */
function reply(res, statusCode, view, ok, html, model, error) {
  res.status(statusCode).render(path.resolve(__dirname, `../views/${view}`), {
    ok: ok,
    html: html,
    model: model,
    error: error
  });
}

const generateHex = size => [...Array(size)].map(() => Math.floor(Math.random() * 16).toString(16)).join('');
const state = generateHex(16);

/**
 * @param {ExpressDataApplication} app
 * @returns {*}
 */
function twitterConnectRouter(app) {
  const service = app.getService(TwitterConnectService);

  const router = Router();
  const passport = new Passport();

  /**
    * @type {{ authorizationURL: string, tokenURL: string, callbackURL: string }}
    */
  if (service.twitterConfig.auth == null) {
    throw new Error('TwitterConnectService configuration may not be null');
  }

  passport.use(new OAuth2Strategy(Object.assign(service.twitterConfig.auth, {
    passReqToCallback: true,
  }), function (req, accessToken, refreshToken, profile, done) {
    /**
      * Gets OAuth2 client services
      * @type {*}
      */
    const client = req.context.getApplication().getStrategy(function OAuth2ClientService() { });
    const translateService = req.context.getApplication().getService(function TranslateService() { });
    // if client cannot be found
    if (typeof client === 'undefined') {
      // throw configuration error
      return done(new Error('Invalid application configuration. OAuth2 client service cannot be found.'))
    }
    if (accessToken == null) {
      // throw 499 Token Required error
      return done(new HttpError(499, 'A token is required to fulfill the request.', 'Token Required'));
    }
    // get token info
    client.getTokenInfo(req.context, accessToken).then(info => {
      if (info == null) {
        // the specified token cannot be found - 498 invalid token with specific code
        return done(new InvalidOrExpiredTokenError(translateService.translate('invalid or expired token')));
      }
      // if the given token is not active throw token expired - 498 invalid token with specific code
      if (!info.active) {
        return done(new InvalidOrExpiredTokenError(translateService.translate('invalid or expired token')));
      }
      return done(null, {
        "name": info.username,
        "authenticationType": 'AccessToken',
        "authenticationToken": accessToken,
        "authenticationScope": info.scope
      });
    }).catch(err => {
      const statusCode = (err && err.statusCode) || 500;
      if (statusCode === 404) {
        // revert 404 not found returned by auth server to 498 invalid token
        return done(new InvalidOrExpiredTokenError(translateService.translate('invalid or expired token')));
      }
      // otherwise continue with error
      return done(err);
    });
  }));

  passport.serializeUser(function (user, done) {
    return done(null, user);
  });

  passport.deserializeUser(function (user, done) {
    return done(null, user);
  });

  // configure rate limit
  const rateLimitOptions = Object.assign({
    windowMs: 5 * 60 * 1000, // 5 minutes
    max: 20, // 20 requests
    standardHeaders: true,
    legacyHeaders: false,
    keyGenerator: (req) => {
      return req.headers['x-real-ip'] || req.headers['x-forwarded-for'] || (req.connection ? req.connection.remoteAddress : req.socket.remoteAddress);
    }
  }, app.getConfiguration().getSourceAt('settings/universis/rateLimit'));
  /**
   * @type {RequestHandler}
   */
  const rateLimitHandler = rateLimit(rateLimitOptions);

  const secret = app.getConfiguration().getSourceAt('settings/crypto/key');

  // use client-side session
  // https://github.com/expressjs/cookie-session#cookie-session
  const sessionCookieOptions = {
    keys: [
      secret
    ],
    name: 'twitter-connect.sid'
  };
  router.use(cookieSession(sessionCookieOptions));

  // initialize passport
  router.use(passport.initialize());
  router.use(passport.session());

  // create context middleware
  router.use((req, _res, next) => {
    // create router context
    const newContext = app.createContext();
    /**
     * try to find if request has already a data context
     * @type {ExpressDataContext|*}
     */
    const interactiveContext = req.context;
    // finalize already assigned context
    if (interactiveContext) {
      if (typeof interactiveContext.finalize === 'function') {
        // finalize interactive context
        return interactiveContext.finalize(() => {
          // and assign new context
          Object.defineProperty(req, 'context', {
            enumerable: false,
            configurable: true,
            get: () => {
              return newContext
            }
          });
          // exit handler
          return next();
        });
      }
    }
    // otherwise assign context
    Object.defineProperty(req, 'context', {
      enumerable: false,
      configurable: true,
      get: () => {
        return newContext
      }
    });
    // and exit handler
    return next();
  });

  // set locale middleware
  router.use((req, _res, next) => {
    // set context locale from request
    req.context.locale = req.locale;
    // set translate
    const translateService = req.context.application.getStrategy(function TranslateService() { });
    req.context.translate = function () {
      if (translateService == null) {
        return arguments[0];
      }
      return translateService.translate.apply(translateService, Array.from(arguments));
    };
    return next();
  });

  router.use((req, _res, next) => {
    req.context.user = req.session.user;
    return next();
  });

  // use this handler to finalize router context
  // important note: context finalization is very important in order
  // to close and finalize database connections, cache connections etc.
  router.use((req, _res, next) => {
    req.on('end', () => {
      //on end
      if (req.context) {
        //finalize data context
        return req.context.finalize(() => {
          //
        });
      }
    });
    return next();
  });

  // handle rate limit
  router.use(rateLimitHandler);

  // login
  router.get('/login', passport.authenticate('oauth2', {
    session: true,
    scope: [
      'profile'
    ]
  }));

  // logout
  router.get('/logout', (req, res, next) => {
    req.session = null;
    return res.status(200).render(path.resolve(__dirname, '../views/logout'));
  });

  router.get('/user', (req, res, next) => {
    if (req.context.user == null) {
      return res.redirect('login');
    }
    return next();
  }, (req, res, next) => {
    return req.context.model('User').where('name').equal(req.context.user.name).expand('twitterAccount').getItem().then((user) => {
      return res.status(200).render(path.resolve(__dirname, '../views/user'), {
        model: {
          logoutURL: service.twitterConfig.auth.logoutURL,
          user: user,
          twitterOAuth2URL: service.twitterAuth.generateAuthURL({ code_challenge_method: 's256', state: state })
        },
        html: {
          context: req.context
        }
      });
    }).catch((err) => {
      return next(err);
    });
  });

  router.get('/disconnect', (req, res, next) => {
    if (req.context.user == null) {
      return res.redirect('login');
    }
    return next();
  }, (req, res, next) => {
    return req.context.model('User').where('name').equal(req.context.user.name).expand('twitterAccount').getItem().then(async (user) => {
      if (user.twitterAccount == null) {
        return res.redirect('user');
      }

      // delete twitter account
      await req.context.model('User').silent().save({
        name: req.context.user.name,
        twitterAccount: null
      });

      res.redirect('user');
    }).catch((err) => {
      return next(err);
    });
  });

  router.get('/callback', passport.authenticate('oauth2', {
    failureRedirect: 'login',
    session: true,
    scope: [
      'profile'
    ]
  }), (req, res, next) => {
    req.context.user = req.session.user = req.user;
    return res.redirect('user');
  });

  router.get('/redirect', async (req, res, next) => {
    let html = {
      context: req.context
    }

    const { code, error, error_description } = req.query;

    if (!code) {
      return reply(res, 400, 'redirect', false, html, null, new HttpBadRequestError('Invalid code.'));
    }

    if (error && error_description) {
      return reply(res, 400, 'redirect', false, html, null, new HttpBadRequestError(error_description));
    }

    try {
      // Exchange code for access token
      const exchange = await service.twitterAuth.requestAccessToken(code);
      if (!exchange?.token?.access_token) {
        return reply(res, 400, 'redirect', false, html, null, new HttpBadRequestError('Something went wrong during the code exchange.'));
      }

      // Get twitter user info
      const twitterUser = await service.client.users.findMyUser();
      await service.twitterAuth.revokeAccessToken();

      if (!twitterUser?.data?.id) {
        return reply(res, 400, 'redirect', false, html, null, new HttpBadRequestError('Invalid user.'));
      }

      // This will NOT send a message from the Twitter bot account to the user.
      // Instead, it will send a message from and to your Twitter account.
      // We're trying to figure out how to send a message from the Twitter bot account to the user.
      
      // fetch(`https://api.twitter.com/2/dm_conversations/with/${twitterUser.data.id}/messages`, {
      //   method: 'POST',
      //   body: JSON.stringify({
      //     "text": "Connected to UniverSIS!"
      //   }),
      //   headers: {
      //     'Content-Type': 'application/json',
      //     'Authorization': `Bearer ${service.twitterConfig.bearer_token}`
      //   }
      // })
      // .then((res) => res.json())
      // .then((res) => {
      //   console.log(res);
      // });

      // Check if universis user has that specific twitter account already connected
      const exists = await req.context.model('User').where('twitterAccount/identifier').equal(twitterUser.data.id).silent().getItem();
      if (exists) {
        return reply(res, 400, 'redirect', false, html, null, new HttpBadRequestError('User already connected.'));
      }
      
      // Save twitter account to universis user
      await req.context.model('User').silent().save({
        name: req.context.user.name,
        twitterAccount: {
          identifier: twitterUser.data.id,
          name: twitterUser.data.username,
        }
      });

      return reply(res, 200, 'redirect', true, html, twitterUser, null)
    } catch (err) {
      return reply(res, 400, 'redirect', false, html, null, err);
    }
  });

  return router;
}

export {
  twitterConnectRouter
}
