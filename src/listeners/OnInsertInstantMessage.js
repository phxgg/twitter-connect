/* eslint-disable no-unused-vars */
import { DataError } from '@themost/common';
import { DataObjectState } from '@themost/data';
import { TwitterConnectService } from '../TwitterConnectService';
// eslint-disable-next-line no-unused-vars
async function beforeSaveAsync(event) {
  //
}

async function afterSaveAsync(event) {
  //
  if (event.state === DataObjectState.Insert) {
    const context = event.model.context;

    const channel = await context.model('MessagingChannel').find(event.target.recipient).expand({ name: 'members', options: { $expand: 'twitterAccount' } }).silent().getItem();

    if (channel) {
      const members = channel.members || [];

      const forwardTo = members.filter((member) => {
        return member.id !== event.target.owner; // do not send message to owner
      });

      for (const member of forwardTo) {
        context.getApplication().getService(TwitterConnectService).sendDirectMessage(member, event.target.body);
      }
    }
  }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
  return beforeSaveAsync(event).then(() => {
    return callback();
  }).catch((err) => {
    return callback(err);
  });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
  return afterSaveAsync(event).then(() => {
    return callback();
  }).catch((err) => {
    return callback(err);
  });
}