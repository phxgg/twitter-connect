/* eslint-disable no-unused-vars */
/* eslint-disable no-console */

import { ApplicationService, TraceUtils } from "@themost/common";
import LocalScopeAccessConfiguration from './config/scope.access.json';
import { AccountReplacer } from './AccountReplacer';
import { InstantMessageReplacer } from './InstantMessageReplacer';
import { twitterConnectRouter } from './routers/twitterConnectRouter';
import { Client as TwitterClient, auth as TwitterAuth } from 'twitter-api-sdk';

/**
 * @param {Router} parent
 * @param {Router} before
 * @param {Router} insert
 */
function insertRouterBefore(parent, before, insert) {
  const beforeIndex = parent.stack.findIndex((item) => {
    return item === before;
  });
  if (beforeIndex < 0) {
    throw new Error('Target router cannot be found in parent stack.');
  }
  const findIndex = parent.stack.findIndex((item) => {
    return item === insert;
  });
  if (findIndex < 0) {
    throw new Error('Router to be inserted cannot be found in parent stack.');
  }
  // remove last router
  parent.stack.splice(findIndex, 1);
  // move up
  parent.stack.splice(beforeIndex, 0, insert);
}

export class TwitterConnectService extends ApplicationService {
  constructor(app) {

    new AccountReplacer(app).apply();
    new InstantMessageReplacer(app).apply();

    super(app);

    /**
     * @type {{
     * app_id: string,
     * api_key: string,
     * api_key_secret: string,
     * client_id: string,
     * client_secret: string,
     * bearer_token: string,
     * redirect_uri: string,
     * scope: string,
     * auth: {}
     * }}
     */
    this.twitterConfig = app.getConfiguration().getSourceAt('settings/universis/twitter-connect');

    try {
      this.twitterAuth = new TwitterAuth.OAuth2User({
        client_id: this.twitterConfig.client_id,
        client_secret: this.twitterConfig.client_secret,
        callback: this.twitterConfig.redirect_uri,
        scopes: this.twitterConfig.scope
      });

      this.client = new TwitterClient(this.twitterAuth);
    } catch (err) {
      console.log('[twitter-connect] Something went wrong while trying to connect to Twitter. Printing error...');
      console.error(err);
    }

    // extend universis api scope access configuration
    if (app && app.container) {
      app.container.subscribe((container) => {
        if (container) {
          // use router
          container.use('/services/twitter', twitterConnectRouter(app));
          // get container router
          const router = container._router;
          // find before position
          const before = router.stack.find((item) => {
            return item.name === 'dataContextMiddleware';
          });
          if (before == null) {
            // do nothing
            return;
          }
          const insert = router.stack[router.stack.length - 1];
          // re-index router
          insertRouterBefore(router, before, insert);

          // add extra scope access elements
          const scopeAccess = app.getConfiguration().getStrategy(function ScopeAccessConfiguration() { });
          if (scopeAccess != null) {
            scopeAccess.elements.push.apply(scopeAccess.elements, LocalScopeAccessConfiguration);
          }
        }
      });
    } else {
      // container is missing
      TraceUtils.warn('TwitterConnectService', 'Application container is missing. Twitter connect service endpoint will be unavailable.');
    }
  }

  async sendDirectMessage(member, body) {
    if (!member.twitterAccount || !member.twitterAccount.identifier) {
      console.log(`[twitter-connect] Member ${member.name} has no Twitter account`);
      return;
    }

    // send direct message
  }

}